import React from 'react';

import Home from '../containers/dashboard/Home';
import { Orders1, Orders2, Orders3 } from '../containers/dashboard/Orders';
import Products from '../containers/dashboard/Products';
import { Analytics1, Analytics2 } from '../containers/dashboard/Analytics';
import Customers from '../containers/dashboard/Customers';
import Reports from '../containers/dashboard/Reports';
import { Users1 } from '../containers/dashboard/Users';
import Integrations from '../containers/dashboard/Integrations';

/* Dashboard Item Example
{
    name: Unique name,
    path: "/unique-path",   || menu []
    component: () => <Component />,
    icon: "icon-name"  (https://feathericons.com/)
}
*/

export const dashboard = [
    {
        name: "Dashboard",
        path: "/home",
        exact: true,
        component: () => <Home />,
        icon: "home"
    },
    {
        name: "Orders",
        menu: [
            {
                name: "Orders1",
                path: "/orders1",
                component: () => <Orders1 />
            },
            {
                name: "Orders2",
                path: "/orders2",
                component: () => <Orders2 />
            },
            {
                name: "Orders3",
                path: "/orders3",
                component: () => <Orders3 />
            }
        ],
        icon: "file"
    },
    {
        name: "Products",
        path: "/products",
        component: () => <Products />,
        icon: "shopping-cart"
    },
    {
        name: "Analytics",
        menu: [
            {
                name: "Analytics1",
                path: "/analytics1",
                component: () => <Analytics1 />
            },
            {
                name: "Analytics2",
                path: "/analytics2",
                component: () => <Analytics2 />
            }
        ],
        icon: "file"
    },
    {
        name: "Customers",
        path: "/customers",
        component: () => <Customers />,
        icon: "users"
    },
    {
        name: "Reports",
        path: "/reports",
        component: () => <Reports />,
        icon: "bar-chart-2"
    },
    {
        name: "Users",
        menu: [
            {
                name: "Users1",
                path: "/users1",
                component: () => <Users1 />
            }
        ],
        icon: "users"
    },
    {
        name: "Integrations",
        path: "/integrations",
        component: () => <Integrations />,
        icon: "layers"
    }
];