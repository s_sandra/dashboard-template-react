import React from 'react';
import FeatherIcon from 'feather-icons-react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import styles from './SidebarLink.module.scss';

function SidebarLink(props) {
    return (
        <li className="nav-item">
            <Link
                className={classNames("nav-link", styles.link, {
                    [styles.active]: props.activeLink === props.linkName
                })}
                to={"/dashboard" + props.linkPath}
                onClick={(e) => props.toggleActiveLink(e, props.linkName, props.menuName)}
            >
                <FeatherIcon icon={props.iconName} className={styles.feather} />
                <span className="px-1">{props.linkName}
                    <span className="sr-only">(current)</span>
                </span>
            </Link>
        </li>
    );
}

SidebarLink.propTypes = {
    activeLink: PropTypes.string,
    linkName: PropTypes.string,
    linkPath: PropTypes.string,
    iconName: PropTypes.string,
    toggleActiveLink: PropTypes.func,
};

export default SidebarLink;
