import React from 'react';

import styles from './Home.module.scss';

function Home() {
    return (
        <div className="centered-container">
            <h1>Home</h1>
        </div>
    );
}

export default Home;
