import React from 'react';
import { Route } from 'react-router-dom';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import Sidebar from '../../containers/Sidebar/Sidebar';
import { dashboard } from '../../utils/dashboard';

import styles from './Dashboard.module.scss';

function Dashboard(props) {

    const routes = dashboard.map((route, index) => {
        if (route.menu) {
            return route.menu.map((subroute, index) => (
                <Route
                    key={index}
                    path={props.match.url + subroute.path}
                    component={subroute.component}
                />
            ));
        } else {
            return <Route
                key={index}
                path={props.match.url + route.path}
                exact={route.exact}
                component={route.component}
            />
        }
    });

    return (
        <div className="row">

            {/* Sidebar */}
            <Sidebar />

            {/* Main Content */}
            <main className={classNames("col-md-10 ml-sm-auto col-lg-10", styles.content)}>
                {routes}
            </main>
        </div>
    );
}

Dashboard.propTypes = {
    match: PropTypes.shape({
        url: PropTypes.string
    })
};

export default Dashboard;