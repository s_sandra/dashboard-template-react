import React from 'react'
import FeatherIcon from 'feather-icons-react'
import PropTypes from 'prop-types'
import classNames from 'classnames';

import SidebarLink from '../SidebarLink/SidebarLink'
import '../SidebarLink/SidebarLink.module.scss';
import styles from './SidebarMenu.module.scss';

function SidebarMenu(props) {

    const toggleActiveMenu = (e) => {
        e.preventDefault();
        if (props.activeMenu === props.linkName) {
            props.toggleActiveLink(e, props.linkName, "");
        } else {
            props.toggleActiveLink(e, props.linkName, props.linkName);
        }
    };

    return (
        <div>
            <li className="nav-item">
                <a className={classNames("nav-link", styles.link, {
                    [styles.activeLink]: props.activeLink === props.linkName
                })}
                    href="/"
                    onClick={toggleActiveMenu}>
                    <FeatherIcon icon={props.iconName} className={styles.feather} />
                    <span className="px-1">{props.linkName}</span>
                    <FeatherIcon icon="chevron-right" className={classNames(styles.arrow, {
                        [styles.activeArrow]: props.activeMenu === props.linkName
                    })} />
                </a>
            </li>
            <div className={classNames(styles.menu, {
                [styles.show]: props.activeMenu === props.linkName
            })}>
                {props.subMenuItems.map((item, index) => {
                    return <SidebarLink
                        activeLink={props.activeLink}
                        linkName={item.name}
                        linkPath={item.path}
                        menuName={props.linkName}
                        toggleActiveLink={props.toggleActiveLink}
                        key={index}
                        iconName=""
                    />
                })}
            </div>
        </div>
    );
}

SidebarMenu.propTypes = {
    activeMenu: PropTypes.string,
    activeLink: PropTypes.string,
    linkName: PropTypes.string,
    linkPath: PropTypes.string,
    toggleActiveLink: PropTypes.func,
    subMenuItems: PropTypes.arrayOf(
        PropTypes.shape({
            name: PropTypes.string,
            path: PropTypes.string,
            component: PropTypes.func
        })
    ),
    iconName: PropTypes.string
};

export default SidebarMenu;