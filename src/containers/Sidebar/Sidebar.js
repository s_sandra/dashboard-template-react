import React from 'react';
import classNames from 'classnames';

import SidebarLink from '../../components/SidebarLink/SidebarLink';
import SidebarMenu from '../../components/SidebarMenu/SidebarMenu';
import { dashboard } from '../../utils/dashboard';

import styles from './Sidebar.module.scss';

class Sidebar extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            activeLink: '',
            activeMenu: ''
        };
    }

    toggleActiveLink = (event, linkName, menuName) => {
        this.setState({
            activeLink: linkName,
            activeMenu: menuName
        });
    }

    renderSidebarLinks = () => {
        return (
            dashboard.map((link, index) => {
                if (link.menu) {
                    return <SidebarMenu
                        activeMenu={this.state.activeMenu}
                        activeLink={this.state.activeLink}
                        linkName={link.name}
                        linkPath={link.path}
                        toggleActiveLink={this.toggleActiveLink}
                        subMenuItems={link.menu}
                        iconName={link.icon}
                        key={index}
                    />
                } else {
                    return <SidebarLink
                        activeLink={this.state.activeLink}
                        linkName={link.name}
                        linkPath={link.path}
                        menuName=""
                        toggleActiveLink={this.toggleActiveLink}
                        iconName={link.icon}
                        key={index}
                    />
                }
            })
        );
    }

    render() {
        return (
            <nav className={classNames("col-md-2", styles.sidebar)}>
                <ul className="nav flex-column">
                    {this.renderSidebarLinks()}
                </ul>
            </nav>
        );
    }
}

export default Sidebar;