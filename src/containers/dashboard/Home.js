import React from 'react';
import { Bar, Doughnut } from 'react-chartjs-2';

class Home extends React.Component {

    constructor(props) {
        super(props);
        this.state = {

        };
    }

    render() {

        const data1 = {
            labels: ["M", "T", "W", "R", "F", "S", "S"],
            datasets: [{
                label: 'apples',
                data: [12, 19, 3, 17, 28, 24, 7],
                backgroundColor: ["#8c9eff", "#8c9eff","#8c9eff", "#8c9eff", "#8c9eff","#8c9eff", "#8c9eff"]
            }, {
                label: 'oranges',
                data: [30, 29, 5, 5, 20, 3, 10],
                backgroundColor: ["#7986cb", "#7986cb", "#7986cb", "#7986cb", "#7986cb", "#7986cb", "#7986cb"] 
            }]
        };

        const data2 = {
            datasets: [{
                data: [30, 20, 10],
                backgroundColor: ["#8c9eff","#7986cb","#c5cae9"]
            }], 
            labels: ['Blue', 'Red', 'Yellow']
        }

        const options2 = {
            legend: {
                position: 'bottom'
            }
        };

        return (
            <div>

                {/* Charts example*/}
                <div className="row">
                    <div className="col-md-6">
                        <div className="dashboard-card">
                            <Bar data={data1} />
                        </div>
                    </div>
                    <div className="col-md-6">
                        <div className="dashboard-card">
                            <Doughnut data={data2} options={options2}/>
                        </div>
                    </div>
                </div>

                {/* Table example */}
                <div className="row">
                    <div className="col-md-12">
                        <div className="dashboard-card">
                            <div className="table-responsive">
                                <table className="table table-bordered table-hover table-sm">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Header</th>
                                            <th>Header</th>
                                            <th>Header</th>
                                            <th>Header</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1,001</td>
                                            <td>Lorem</td>
                                            <td>ipsum</td>
                                            <td>dolor</td>
                                            <td>sit</td>
                                        </tr>
                                        <tr>
                                            <td>1,002</td>
                                            <td>amet</td>
                                            <td>consectetur</td>
                                            <td>adipiscing</td>
                                            <td>elit</td>
                                        </tr>
                                        <tr>
                                            <td>1,003</td>
                                            <td>Integer</td>
                                            <td>nec</td>
                                            <td>odio</td>
                                            <td>Praesent</td>
                                        </tr>
                                        <tr>
                                            <td>1,003</td>
                                            <td>libero</td>
                                            <td>Sed</td>
                                            <td>cursus</td>
                                            <td>ante</td>
                                        </tr>
                                        <tr>
                                            <td>1,004</td>
                                            <td>dapibus</td>
                                            <td>diam</td>
                                            <td>Sed</td>
                                            <td>nisi</td>
                                        </tr>
                                        <tr>
                                            <td>1,005</td>
                                            <td>Nulla</td>
                                            <td>quis</td>
                                            <td>sem</td>
                                            <td>at</td>
                                        </tr>
                                        <tr>
                                            <td>1,006</td>
                                            <td>nibh</td>
                                            <td>elementum</td>
                                            <td>imperdiet</td>
                                            <td>Duis</td>
                                        </tr>
                                        <tr>
                                            <td>1,007</td>
                                            <td>sagittis</td>
                                            <td>ipsum</td>
                                            <td>Praesent</td>
                                            <td>mauris</td>
                                        </tr>
                                        <tr>
                                            <td>1,008</td>
                                            <td>Fusce</td>
                                            <td>nec</td>
                                            <td>tellus</td>
                                            <td>sed</td>
                                        </tr>
                                        <tr>
                                            <td>1,009</td>
                                            <td>augue</td>
                                            <td>semper</td>
                                            <td>porta</td>
                                            <td>Mauris</td>
                                        </tr>
                                        <tr>
                                            <td>1,010</td>
                                            <td>massa</td>
                                            <td>Vestibulum</td>
                                            <td>lacinia</td>
                                            <td>arcu</td>
                                        </tr>
                                        <tr>
                                            <td>1,011</td>
                                            <td>eget</td>
                                            <td>nulla</td>
                                            <td>className</td>
                                            <td>aptent</td>
                                        </tr>
                                        <tr>
                                            <td>1,012</td>
                                            <td>taciti</td>
                                            <td>sociosqu</td>
                                            <td>ad</td>
                                            <td>litora</td>
                                        </tr>
                                        <tr>
                                            <td>1,013</td>
                                            <td>torquent</td>
                                            <td>per</td>
                                            <td>conubia</td>
                                            <td>nostra</td>
                                        </tr>
                                        <tr>
                                            <td>1,014</td>
                                            <td>per</td>
                                            <td>inceptos</td>
                                            <td>himenaeos</td>
                                            <td>Curabitur</td>
                                        </tr>
                                        <tr>
                                            <td>1,015</td>
                                            <td>sodales</td>
                                            <td>ligula</td>
                                            <td>in</td>
                                            <td>libero</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        );
    }
}

export default Home;