import React from 'react';
import { connect } from 'react-redux';
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect
} from 'react-router-dom';
import PropTypes from 'prop-types';

import Header from './Header/Header';
import Home from '../components/Home/Home';
import Login from './Login/Login';
import Signup from './Signup/Signup';
import Dashboard from '../components/Dashboard/Dashboard';
import * as Actions from '../actions/index';

import logo from '../logo.svg';

const PrivateRoute = ({ component: Component, authenticated, ...rest }) => {
  return (
    <Route
      {...rest}
      render={(props) => authenticated === true
        ? <Component {...props} />
        : <Redirect to={{
          pathname: '/login',
          state: { from: props.location }
        }} />}
    />
  );
}

const PublicRoute = ({ component: Component, authenticated, ...rest }) => {
  return (
    <Route
      {...rest}
      render={(props) => authenticated === false
        ? <Component {...props} />
        : <Redirect to='/dashboard/home' />}
    />
  );
}

class App extends React.Component {

  componentDidMount() {
    if (sessionStorage.getItem("auth")) {
      this.props.signInUser();
    } else {
      this.props.signOutUser();
    }
  }

  render() {
    return (
      <Router>
        <Header />
        <div className="container-fluid">
          <Switch>
            <Route
              exact
              path="/"
              component={Home}
            />
            <PublicRoute
              path="/login"
              component={Login}
              authenticated={this.props.authenticated}
            />
            <PublicRoute
              path="/signup"
              component={Signup}
              authenticated={this.props.authenticated}
            />
            <PrivateRoute
              path="/dashboard"
              component={Dashboard}
              authenticated={this.props.authenticated}
            />
            <Route component={Home} />
          </Switch>
        </div>
      </Router>
    );
  }
}

function mapStateToProps(state) {
  return {
    authenticated: state.auth.authenticated
  }
}

App.propTypes = {
  authenticated: PropTypes.bool,
  signInUser: PropTypes.func,
  signOutUser: PropTypes.func
};

export default connect(mapStateToProps, Actions)(App);
