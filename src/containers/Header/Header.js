import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import FeatherIcon from 'feather-icons-react';

import * as Actions from '../../actions/auth';

import styles from './Header.module.scss';

class Header extends React.Component {

    handleSignOut = () => {
        this.props.signOutUser();
    }

    renderAuthLinks = () => {
        if (this.props.authenticated) {
            return [
                <li className="nav-item" key={1}>
                    <Link
                        to="/dashboard/home"
                        className={classNames("nav-link", styles.link)}>
                        Profile
                    </Link>
                </li>,
                <li className="nav-item" key={2}>
                    <Link
                        to=""
                        className={classNames("nav-link", styles.link)}
                        onClick={this.handleSignOut}>
                        Sign out
                    </Link>
                </li>
            ]
        } else {
            return [
                <li className="nav-item" key={1}>
                    <Link
                        to="/login"
                        className={classNames("nav-link", styles.link)}>
                        Login
                    </Link>
                </li>,
                <li className="nav-item" key={2}>
                    <Link
                        to="/signup"
                        className={classNames("nav-link", styles.link)}>
                        Sign up
                    </Link>
                </li>
            ]
        }
    }

    render() {
        return (
            <nav className={classNames("navbar navbar-expand-md navbar-light", styles.mainNav)}>
                <Link
                    to="/"
                    className={classNames("navbar-brand", styles.brand)}>
                    <FeatherIcon icon="aperture" className={styles.brandIcon}/>
                    <span className={styles.brandText}>Company name</span>
                </Link>
                <button
                    className={classNames("navbar-toggler", styles.toggler)}
                    type="button"
                    data-toggle="collapse"
                    data-target="#navbarCollapse"
                    aria-controls="navbarCollapse"
                    aria-expanded="false"
                    aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarCollapse">
                    <ul className={classNames("navbar-nav", styles.list)}>
                        {this.renderAuthLinks()}
                    </ul>
                </div>
            </nav>
        );
    }
}

function mapStateToProps(state) {
    return {
        authenticated: state.auth.authenticated
    }
}

Header.propTypes = {
    authenticated: PropTypes.bool,
    signOutUser: PropTypes.func
};

export default connect(mapStateToProps, Actions)(Header);
