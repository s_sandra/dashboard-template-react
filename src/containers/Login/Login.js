import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import * as Actions from '../../actions/auth';

import styles from './Login.module.scss';

class Login extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: ''
        };
    }

    handleChange = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        });
    }


    handleSubmit = (event) => {
        event.preventDefault();
        this.props.signInUser(this.state);
    }

    render() {
        return (
            <div className="centered-container">
                <form className={classNames("form", styles.form)}>
                    <h4 className={styles.title}>Login</h4>
                    <input
                        type="email"
                        name="email"
                        value={this.state.email}
                        className={classNames("form-control", styles.input)}
                        placeholder="Email address"
                        required
                        autoFocus
                        onChange={this.handleChange}
                    />
                    <input
                        type="password"
                        name="password"
                        value={this.state.password}
                        className={classNames("form-control", styles.input)}
                        placeholder="Password"
                        required
                        onChange={this.handleChange}
                    />
                    <div className={styles.row}>
                        <button
                            className={classNames("btn btn-primary", styles.button)}
                            type="submit"
                            onClick={this.handleSubmit}>
                            Login
                        </button>
                    </div>
                </form>
            </div>
        )
    }
}

Login.propTypes = {
    signInUser: PropTypes.func
};

export default connect(null, Actions)(Login);