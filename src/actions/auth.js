export const SIGN_IN_USER = 'SIGN_IN_USER';
export const SIGN_OUT_USER = 'SIGN_OUT_USER';

export function signInUser(values){
    console.log(values);
    sessionStorage.setItem("auth", "true");
    return{
        type: SIGN_IN_USER
    };
}

export function signOutUser(){
    sessionStorage.removeItem("auth");
    return{
        type: SIGN_OUT_USER
    };
}